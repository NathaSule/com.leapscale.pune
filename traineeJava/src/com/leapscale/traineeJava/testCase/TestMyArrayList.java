package com.leapscale.traineeJava.testCase;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.leapscale.traineeJava.ArrayListModel.MyArrayList;

public class TestMyArrayList {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("tearDownAfterClass");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("setUp");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown");
	}

	@Test
	public void test() {
		MyArrayList arr = new MyArrayList();
		arr.add(10);
		arr.add(20);
		arr.add(30);
		arr.add(40);
		for (int i = 0; i < arr.size(); i++) {
			System.out.println(arr.get(i));
		}
		
		int data = arr.get(1);
		
		if(data==20)
			assert(true);
		else
			assert(false);
	}

}
