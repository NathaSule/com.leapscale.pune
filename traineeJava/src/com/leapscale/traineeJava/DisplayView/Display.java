package com.leapscale.traineeJava.DisplayView;

import com.leapscale.traineeJava.ArrayListModel.MyArrayList;
import com.leapscale.traineeJava.InputController.MyScanner;

public class Display {
	
	static int max=0,min=0;
	static MyArrayList list = new MyArrayList(); // MyArrayList Initialization
	
	/*
	 * Printing MyArrayList*/
	public void originalArray(){
		
		
		min = list.get(0); // Initial min and max Will be Same
		max = list.get(0);
		for (int i = 0; i < list.size(); i++) 
		{
			int data = list.get(i);
			if (data > max )
				max = data;
			else if(data < min)
				min = data;
			System.out.print(i+"th Position::");
			System.out.println(list.get(i));
		}
	}
	
	/*
	 * Printing Min and Max Value
	 * from MyArrayList */
	
	public void minMax() {  
		System.out.println("max is:: "+max);
		System.out.println("min is:: "+min);
	}
	/*
	 * Iterating Over MyArrayList items
	 * with the help of User Choice*/
	public void getuserChoice() {
		int choice = 0;
		try 
		{
			
			while((choice = MyScanner.user_Menu()) != 0) // execute until choice becomes 0 
			{
				switch(choice)
				{
				case 1:
					int size = MyScanner.numberOfElement(); // No. of items to be added 
					for (int i = 0; i < size; i++) 
					{
						
						int data = MyScanner.addElement(); // actual items adding in MyArrayList
						list.add(data);
					}
					break;
				case 2:
					int pos = MyScanner.indexforRemove(); // Position to Remove item
					try {
						list.remove(pos);
					} catch (ArrayIndexOutOfBoundsException e) {
						System.out.println("Invalid Position to Remove item");
						System.out.println("Please Enter Valid Position");
					}
					break;
				case 3:
					originalArray(); // just display MyArrayList items
					minMax();
					break;
				default:
					break;
				}			
			}
		} 
		catch (Exception e) // handling exception //NumberFormatException |
		{}
		finally {
			MyScanner.getScanner().close();
		}
	}

}
