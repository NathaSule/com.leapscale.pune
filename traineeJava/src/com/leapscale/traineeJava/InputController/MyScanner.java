package com.leapscale.traineeJava.InputController;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.leapscale.traineeJava.DisplayView.Display;

public class MyScanner 
{
	public static Scanner sc =new Scanner(System.in); 
	
	public static Scanner getScanner() // getting Scanner Object/instance
	{
		if (sc == null) {
			sc = new Scanner(System.in);
			System.out.println("new sc object");
		}
		return sc;
	}
	
	public static int user_Menu() // Menu Driven List
	{	
		System.out.println("0. For Exit");
		System.out.println("1. For Add Element to Array");
		System.out.println("2. For Remove Element from Array");
		System.out.println("3. For Display");
		
		return validateInput();
	}
	
	private static int validateInput()
	{
		int choice = 0;
		String data = getScanner().next();
		
		boolean	val = isInteger(data);
			if (val) {
				choice = Integer.parseInt(data);
			}
			else {
				Display dis = new Display();
				dis.getuserChoice();
			}
			return choice;
	}
	
	private static boolean isInteger(String data) {
		
		try {
			Integer.parseInt(data);
		} catch (InputMismatchException | NumberFormatException e) {
			System.out.println("Please Enter Valid Data::???");
			return false;
		}
		return true;
		
	}

	/*
	 * Number of items which u want to add
	 * in MyArrayList*/
	public static int numberOfElement() { 
		System.out.println("How many elements do u want to ADD::");
		return validateInput();//getScanner().nextInt();
	}
	/*
	 * Remove item from MyArrayList
	 * by entering position */
	public static int indexforRemove() {
		System.out.println("Enter the Position to REMOVIE::");
		return validateInput();//getScanner().nextInt();
	}

	public static int addElement() {
		System.out.println("Enter Element::");
		return validateInput();//getScanner().nextInt();
	}
}
