package com.leapscale.traineeJava.ArrayListModel;

public class MyArrayList {
	
	private Integer[] mySize; // Data Member of MyArrayList
	private int actSize = 0;
	
	public MyArrayList() 
	{
		// Initially Array Will be initialize to Size 10
		mySize = new Integer[10];
	}
	
	/*
	 * Get the item
	 * by using index of the Array*/
	public Integer get(int index) 
	{
		if(index < actSize)
			return mySize[index];
		else
			throw new ArrayIndexOutOfBoundsException();
	}
	/*
	 * Add the item 
	 * into Array*/
	public void add(Integer obj)
	{
		if(mySize.length - actSize <= 5)
			increaseListSize();
		mySize[actSize++] = obj;
	}
	
	/*
	 * Remove the item from Array
	 * by using position*/
	public void remove(int pos) {
		
		if(pos < actSize){
			decreaseSize(pos);
		}
		else
			throw new ArrayIndexOutOfBoundsException();
	}
	/*
	 * Decrease the Array Size after
	 * removing item from Array*/
	private void decreaseSize(int pos) {
		mySize = copyfromPosition(pos,actSize, (mySize.length)-1);
	}

	/*
	 * Shift the Array 
	 * From Removing Position by 1*/
	private Integer[] copyfromPosition(int pos,int array, int newSize) {

		mySize[pos] = null;
		for (int i = pos; i < array; i++) {
			mySize[i] = mySize[i+1];
		}
		actSize--;
		System.out.println("Removed Successfully...");
		return mySize; 
	}

	public int size() // get the Size of Array
	{
		return actSize;
	}
	/*
	 * Increase the Size of Array 
	 * by it's current size * 2*/
	private void increaseListSize() 
	{	
		mySize = copy(mySize , mySize.length * 2);//Arrays.copyOf(mySize, mySize.length * 2);
		System.out.println("New Length :: "+mySize.length);
	}
	/*
	 * Need to Copy the Array
	 * while increasing the size of Array*/
	Integer[] copy(Integer[] array,int newSize)
	{
		Integer[] newLength = new Integer[newSize];
		for (int i = 0; i < array.length; i++) {
			newLength[i] = mySize[i];
		}
		return newLength; 
	}

}
