package com.Leapscale.Trainee.BusinessLogic;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.BitSet;

import javax.swing.JOptionPane;

import com.Leapscale.Trainee.DTandDD.DrivingSession;
import com.Leapscale.Trainee.ReadFile.Constant;
import com.Leapscale.Trainee.ReadFile.ReadCSVFile;
import com.Leapscale.Trainee.WriteFile.WriteCSVFile;

public class DriverConnect {

	int flag = -1;
	final BitSet flags = new BitSet(7);

	ReadCSVFile readfile ;
	WriteCSVFile writefile;

	ArrayList<DrivingSession> session;

	int onCount = 0;
	int offCount = 0;

	int start_session = 0;
	int end_session = 0;

	static int index = 0;

	public void MaintainSession(String readFilePath,String writeFilePath) {

		try {

			readfile = new ReadCSVFile(readFilePath);
			writefile =  new WriteCSVFile(writeFilePath);
			session = new ArrayList<DrivingSession>();

			int SIZE = readfile.numberOfRecordInCSVFile();

			for (int i = 0; i < SIZE; i++) 
			{
				index = i;
				String line = readfile.getRecordOfIndex(index);
				String[] result = splitProvidedLine(line);

				session.add(new DrivingSession(0.0, 0.0, getIntegerValue(result[1]), getIntegerValue(result[3]), getLongValue(result[0]), 0.0, 0));



				//onDriveChecking
				if (session.get(index).getSpeed() >= Constant.X1 && onCount >= Constant.Y1) 
				{
					// Maintain OFF Driving Session
					if (flag == 0) 
					{
						maintainOffDrivingSession();
					}

					enableFlag(0);
					flag = 1;
				}
				else if(session.get(index).getSpeed() >= Constant.X1 && onCount < Constant.Y1)
				{
					onCount++;
					offCount = 0;
				}
				else if (session.get(i).getSpeed() < Constant.X1 && session.get(i).getSpeed() >= Constant.X2) 
				{
					if (checkFlag(0)) // Came In-Between From-ON Driving
						flag = 1;
					else			// Came In-Between From-OFF Driving
						flag = 0;

					onCount = 0;
					offCount = 0;	
				}



				//offDriveChecking
				else if (session.get(i).getSpeed() < Constant.X2 && offCount >= Constant.Y2)
				{
					// Maintain ON Driving Session
					if (flag == 1) 
					{
						maintainOnDrivingSession();

					}
					disableFlag(0);
					onCount = 0;
					flag = 0;
				}
				else if (session.get(index).getSpeed() < Constant.X2 && offCount <= Constant.Y2) 
				{
					offCount++;
					onCount = 0;
				}
				else
					JOptionPane.showMessageDialog(null, "Something Went Wrong...");
			}

			// At the end Read Last Record and Maintain End Session
			if (checkFlag(0)) 
			{

				DrivingSession end_Result = session.get(SIZE - 1); // Find End
				DrivingSession start_Result = session.get(start_session);
				
				DrivingSession error_Result0 = session.get(0);
				long error = (long)(end_Result.getTime() - error_Result0.getTime());
				
				if (error != SIZE-1) {
					System.out.println("There is Jumping Error In File");
				}

				writefile.writeInCSVFile(getOnDrive(), start_Result.getTime(), end_Result.getTime(), start_Result.getOdoMeter(), end_Result.getOdoMeter());				
			}
			else {

				DrivingSession end_Result = session.get(SIZE - 1); // Find End	
				DrivingSession start_Result = session.get(start_session);

				writefile.writeInCSVFile(getOffDrive(), start_Result.getTime(), end_Result.getTime(), start_Result.getOdoMeter(), end_Result.getOdoMeter());				
			}

			// Show Message to User that Record is Saved.
			JOptionPane.showMessageDialog(null, "Record Saved...\nPlease Check Below File\n VehicleOutPutData.csv");

		} catch (Exception e) {
			System.out.println("There maybe garbage Value in CSV File");
		}
	}


	/*
	 * On Driving
	 * Maintained */
	private void maintainOnDrivingSession()
	{

		end_session = index-(Constant.Y2+1); // Find End

		DrivingSession end_Result = session.get(end_session);
		DrivingSession start_Result = session.get(start_session);

		writefile.writeInCSVFile(getOnDrive(), start_Result.getTime(), end_Result.getTime(), start_Result.getOdoMeter(), end_Result.getOdoMeter());
		start_session = index - Constant.Y2; // Find Start
	}
	/*
	 * Off Driving
	 * Maintained */
	private void maintainOffDrivingSession() 
	{
		end_session = index-(Constant.Y1+1); // Find End

		DrivingSession end_Result = session.get(end_session);
		DrivingSession start_Result = session.get(start_session);

		writefile.writeInCSVFile(getOffDrive(), start_Result.getTime(), end_Result.getTime(), start_Result.getOdoMeter(), end_Result.getOdoMeter());
		start_session = index - Constant.Y1; // Find Start

	}



	/*
	 * Convert String Data
	 * Into Appropriate type */
	private  long getLongValue(String value) {
		return Long.parseLong(value);
	}
	private  int getIntegerValue(String value){
		return Integer.parseInt(value);
	}


	/*
	 * Split One Line Record */
	private  String[] splitProvidedLine(String line) {
		return line.split(",");
	}



	/*
	 * Get the Drive
	 * Status */
	private  String getOnDrive() {
		return "onDrive";
	}
	private  String getOffDrive() {
		return "offDrive";
	}



	/*
	 * Maintain and Check
	 * flag Status*/
	private void enableFlag(int i) {
		flags.set(i);
	}
	private void disableFlag(int i) {
		flags.set(i, false);
	} 
	private boolean checkFlag(int i) {
		return flags.get(i);
	}
}
