package com.Leapscale.Trainee.WriteFile;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import com.Leapscale.Trainee.DTandDD.Main;

public class WriteCSVFile {

	public  String writeFilePath;

	/*
	 * Parameterized Constructor
	 * for path of writing CSV file*/
	public WriteCSVFile(String writeFilePath)
	{
		this.writeFilePath = writeFilePath;
	}

	/*
	 * write the session
	 * in CSV File*/
	public void writeInCSVFile(String driveStatus,long startTime,long endTime,double startOdo,double endOdo) 
	{

		double distance = endOdo - startOdo;

		System.out.println("start:"+ startTime+ ", end:"+ endTime+", Dist: "+ distance +"km"+ ","+ driveStatus);

		try {
			FileWriter fw = new FileWriter(writeFilePath+"/VehicleOutPutData.csv",true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			pw.println("start:"+ startTime+ ", end:"+ endTime+", Dist: "+ distance +"km"+ ","+ driveStatus);
			pw.flush();
			pw.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Record Not Saved...");
			System.out.println("Please...\nProvide Correct OutPut FilePath");
			Main.startWorkingOnFile();
		}		
	}


}
