package com.Leapscale.Trainee.DTandDD;

public class DrivingSession 
{
	double latitude;
	double longitude;

	float speed;
	double odoMeter;
	long time;
	double bearing;
	int direction;

	public DrivingSession() {
		latitude = 0.0;
		longitude = 0.0;
		speed = 0;
		odoMeter = 0.0;
		time = 0;
		bearing = 0.0;
		direction = 0;
	}
	public DrivingSession(double latitude, double longitude, float speed, double odoMeter, long time, double bearing,
			int direction) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.speed = speed;
		this.odoMeter = odoMeter;
		this.time = time;
		this.bearing = bearing;
		this.direction = direction;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public double getOdoMeter() {
		return odoMeter;
	}
	public void setOdoMeter(double odoMeter) {
		this.odoMeter = odoMeter;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public double getBearing() {
		return bearing;
	}
	public void setBearing(double bearing) {
		this.bearing = bearing;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}

}
