package com.Leapscale.Trainee.DTandDD;


import java.util.Scanner;

import javax.swing.JOptionPane;

import com.Leapscale.Trainee.BusinessLogic.DriverConnect;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		startWorkingOnFile();

	}

	public static void startWorkingOnFile() 
	{
		System.out.println("Please Provide... \n  Input Path with Name of Your CSV File");
		System.out.println("Which Contains... \n  <Time>,<Vehicle Speed>,<Engine Speed>,<OdoMeter>");

		System.out.println("For Example- /home/nsule/Natha/VehicleData.csv");

		try (Scanner sc = new Scanner(System.in))
		{
			String readline = sc.nextLine();
			System.out.println("Please Provide Output Path");
			System.out.println("For Example- /home/nsule/Natha");
			String writeLine = sc.nextLine();
			DriverConnect driverConnect = new DriverConnect();
			driverConnect.MaintainSession(readline,writeLine);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Please...\nProvide Correct FilePath");
		}
		
	}

}
