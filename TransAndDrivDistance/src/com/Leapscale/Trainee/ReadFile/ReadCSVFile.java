package com.Leapscale.Trainee.ReadFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JOptionPane;

import com.Leapscale.Trainee.DTandDD.Main;

public class ReadCSVFile {


	List<String> lines;


	/*
	 * Read the CSV File
	 * by given file-path*/
	public ReadCSVFile(String filepath) 
	{
		try 
		{
			lines = Files.readAllLines(Paths.get(filepath)); //"src/com/Leapscale/InputFile/VehicleData.csv"
		} catch (IOException e) 
		{
			System.out.println("Error While Reading File");
			System.out.println("Please...\nProvide Correct Input FilePath");
			JOptionPane.showMessageDialog(null, "Please...\nProvide Correct FilePath");
			Main.startWorkingOnFile();
		}
	}

	/*
	 * get the Size
	 * of CSV File*/
	public int numberOfRecordInCSVFile(){
		return lines.size();
	}

	/*
	 * get record 
	 * of particular index*/
	public String getRecordOfIndex(int lineNumber) {

		return lines.get(lineNumber);
	}
}
