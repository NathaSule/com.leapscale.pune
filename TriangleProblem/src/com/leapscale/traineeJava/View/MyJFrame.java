package com.leapscale.traineeJava.View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.leapscale.traineeJava.Model.TriangleShape;

public final class MyJFrame{
	private JFrame frame;
	TriangleShape triangle;
	JPanel mytriangle;
	JLabel point1,point2,point3,labelError;
	
	String side1,side2,side3;
	float ab,bc,ac;
	
	public MyJFrame() {
		prepareGUI(); // UI initialization
		
	}
	private void prepareGUI() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 400);
		frame.setLayout(null);
		JLabel label = new JLabel("",JLabel.CENTER);
		
		 point1 = new JLabel();
		 point2 = new JLabel();
		 point3 = new JLabel();
		 labelError = new JLabel();
		
		final JTextField side1Text = new JTextField(6);
		final JTextField side2Text = new JTextField(6); 
		final JTextField side3Text = new JTextField(6); 

	      JButton loginButton = new JButton("Draw Triangle"); // on Click Button action
	      loginButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 side1 = side1Text.getText();
	             side2 = side2Text.getText();
	             side3 = side3Text.getText();
	            
	             	point1.setText("");
					point2.setText("");
					point3.setText("");
					labelError.setText("");
	             
	            drawTriangle(side1,side2,side3); //Redraw Triangle
			}
		}); 
		  label.setText("Please Enter the 3 Sides of Triangle");
		  label.setSize(300,50);
		  label.setForeground(Color.blue);
		  
	      side1Text.setBounds(40, 50, 50, 30); // set the Field on JFrame (x,y,width,height)
	      labelError.setSize(400,500);
	      side2Text.setBounds(40, 90, 50, 30);	
	      side3Text.setBounds(40, 130, 50, 30);
	      loginButton.setBounds(40, 170, 140, 40);
	      
	      frame.add(labelError);
	      frame.add(point1);
	      frame.add(point2);
	      frame.add(point3);
	      frame.add(label);
	      frame.add(side1Text);
	      frame.add(side2Text);
	      frame.add(side3Text);
	      frame.add(loginButton);
	      frame.setVisible(true); // call only one time
	}
	
	public void drawTriangle(String side1, String side2, String side3)
	{
		try {
			ab = Float.valueOf(side1);
			bc = Float.valueOf(side2);
			ac = Float.valueOf(side3);
		} catch (NumberFormatException e) 
		{}
		float Dab = ab*ab;
		float Dac = ac*ac;
		float Dbc = bc*bc;
			
		float Ax = 0;
		float Ay = 0;
		float Bx = 0;
		float By = ab;
		
		float Cy = ((Dab + Dac - Dbc) / (2 * ab)); // calculate value of third Coordinate
		float Cx = (float)Math.sqrt(Dac-Cy*Cy);
		
		if ( (ab >= (bc+ac) ) || (bc >= (ab+ac) ) || (ac >= (ab+bc)) ) 
		{
			System.out.println("Invalid Input...???");
			labelError.setText("Invalid Input");
		}
		else
		{
			
			if (ab==bc && bc==ac && ab==ac)
				labelError.setText("Equilateral Triangle");
			else if (ab==bc || bc==ac || ab==ac)
				labelError.setText("Isosceles Triangle");
			else
				labelError.setText("Scalene Triangle");
			
			triangle = new TriangleShape(new Point2D.Double(Ax, Ay),new Point2D.Double(Bx,By), new Point2D.Double(Cx,Cy)); // Initialize triangle shape
			
			
	
		mytriangle = new JPanel(){ //Local Class 
			
			@Override
			public Dimension getPreferredSize() {
				return new Dimension(400,400);
			}
			
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2d = (Graphics2D)g.create();
					
				point1.setBounds(Math.round(Ax)+40, Math.round(Ay)+40, 10, 10);
				point2.setBounds(Math.round(Bx)+40, Math.round(By)+40, 10, 10);
				point3.setBounds(Math.round(Cx)+48, Math.round(Cy)+48, 10, 10);
				
				point1.setText("A");
				point2.setText("B");
				point3.setText("C");
				
					g2d.setColor(Color.RED);
					g2d.translate(50, 50);
					g2d.fill(triangle);
					g2d.dispose();
			}
		};
		mytriangle.setBounds(200, 200, 600, 600);
		mytriangle.add(point1);
		mytriangle.add(point2);
		mytriangle.add(point3);
		frame.add(mytriangle);
		mytriangle.repaint();
		
		frame.setVisible(true);
		}
	}
	
}
